package com.thisislofty.challeges.model;

public enum ColorEnum {
    RED,
    ORANGE,
    YELLOW,
    GREEN,
    CYAN,
    BLUE,
    PURPLE
}
