package com.thisislofty.challeges.model;

public enum BageShapeType {
    CIRCLE,
    SQUARE,
    STAR,
    SHIELD,
    MEDAL,
    PENTAGON,
    HEXAGON,
    OCTAGON
}
