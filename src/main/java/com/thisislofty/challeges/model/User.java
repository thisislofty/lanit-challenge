package com.thisislofty.challeges.model;

import java.util.List;

public class User {
    private String id;
    private String name;
    private String photo;
    private int level;
    private int exp;
    private boolean isMaster;
    private List<Challenge> achievements;
    private List<Challenge> activeChallenges;
    private List<Challenge> createdChallenges;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public boolean isMaster() {
        return isMaster;
    }

    public void setMaster(boolean master) {
        isMaster = master;
    }

    public List<Challenge> getAchievements() {
        return achievements;
    }

    public void setAchievements(List<Challenge> achievements) {
        this.achievements = achievements;
    }

    public List<Challenge> getActiveChallenges() {
        return activeChallenges;
    }

    public void setActiveChallenges(List<Challenge> activeChallenges) {
        this.activeChallenges = activeChallenges;
    }

    public List<Challenge> getCreatedChallenges() {
        return createdChallenges;
    }

    public void setCreatedChallenges(List<Challenge> createdChallenges) {
        this.createdChallenges = createdChallenges;
    }
}
