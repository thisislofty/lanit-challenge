package com.thisislofty.challeges.model;

public enum ChallengeType {
    WORK,
    EDUCATION,
    SPORT,
    FUN,
    MENTORSHIP,
    OTHER
}
