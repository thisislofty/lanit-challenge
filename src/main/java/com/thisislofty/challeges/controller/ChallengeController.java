package com.thisislofty.challeges.controller;

import com.thisislofty.challeges.model.Challenge;
import com.thisislofty.challeges.service.ChallengeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/challenge")
public class ChallengeController {

    @Autowired
    private ChallengeService service;

    @GetMapping("")
    public Challenge get(@RequestParam String id) {
        return service.getById(id);
    }

    @GetMapping("/list")
    public List<Challenge> list() {
        return service.getAll();
    }

    @PostMapping("/create")
    public boolean create(@RequestBody Challenge challenge){
        service.save(challenge);
        return true;
    }

    @GetMapping("/terminate")
    public void deleteAll(){
        service.deleteAll();
    }
}
