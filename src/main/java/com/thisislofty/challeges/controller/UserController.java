package com.thisislofty.challeges.controller;

import com.thisislofty.challeges.model.Challenge;
import com.thisislofty.challeges.model.User;
import com.thisislofty.challeges.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService service;

    @PostMapping("/create")
    public void createUser(User user){
        service.create(user);
    }

    @GetMapping("")
    public User getUser (@RequestParam String id){
        return service.getById(id);
    }

    @GetMapping("/challenges")
    public List<Challenge> getUserChallenges (){
        return service.getChallengesById("1");
    }

    @GetMapping("/take-challenge")
    public boolean takeChallenge (@RequestParam String challengeId){
        return service.takeChallenge("1", challengeId);
    }

}
