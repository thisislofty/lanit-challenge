package com.thisislofty.challeges;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChallegesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChallegesApplication.class, args);
	}

}
