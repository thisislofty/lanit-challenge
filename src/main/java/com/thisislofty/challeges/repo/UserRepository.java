package com.thisislofty.challeges.repo;

import com.thisislofty.challeges.model.User;
import org.bson.types.ObjectId;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, String> {
    User getById(String id);
}
