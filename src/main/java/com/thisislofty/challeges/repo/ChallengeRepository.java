package com.thisislofty.challeges.repo;

import com.thisislofty.challeges.model.Challenge;
import org.springframework.data.repository.CrudRepository;

public interface ChallengeRepository extends CrudRepository<Challenge, String> {

    Challenge getById(String id);

}
