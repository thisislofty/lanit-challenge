package com.thisislofty.challeges.service;

import com.thisislofty.challeges.model.Challenge;
import com.thisislofty.challeges.model.User;
import com.thisislofty.challeges.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    @Autowired
    ChallengeService challengeService;

    @Autowired
    UserRepository repo;

    public boolean create(User user) {
        return null != repo.save(user);
    }

    public User getById(String id) {
        if (StringUtils.isEmpty(id)) return null;
        return repo.getById(id);
    }

    public boolean takeChallenge(String userId, String challengeId) {
        Challenge challenge = challengeService.getById(challengeId);
        User user = repo.getById(userId);
        if(challenge!=null && user != null){
            if(challenge.getParticipantsIds()==null)challenge.setParticipantsIds(new ArrayList<>());
            challenge.getParticipantsIds().add(user.getId());
            if(user.getActiveChallenges() == null) user.setActiveChallenges(new ArrayList<>());
            user.getActiveChallenges().add(challenge);
            repo.save(user);
            challengeService.save(challenge);
            return true;
        } else {
            return false;
        }
    }

    public List<Challenge> getChallengesById(String s) {
        User user = repo.getById(s);
        return user.getActiveChallenges();
    }
}
