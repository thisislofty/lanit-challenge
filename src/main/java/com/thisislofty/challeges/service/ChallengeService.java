package com.thisislofty.challeges.service;

import com.thisislofty.challeges.model.Challenge;
import com.thisislofty.challeges.repo.ChallengeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ChallengeService {

    @Autowired
    private ChallengeRepository repo;

    public Challenge getById(String id){
        return repo.getById(id);
    }

    public Challenge update (Challenge challenge){
        return repo.save(challenge);
    }

    public List<Challenge> getAll (){
        List<Challenge> res = new ArrayList<>();
        repo.findAll().forEach(res::add);
        return res;
    }

    public void save(Challenge challenge) {
//        challenge.setImg(getImgStr(challenge.getImg()));
        repo.save(challenge);
    }

    private String getImgStr(String img) {
        String[] split = img.split("/");
        return split[split.length-2]+"/"+split[split.length-1];
    }

    public void deleteAll() {
        repo.deleteAll();
    }
}
